<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h2>Sign Up Form</h2>

        <form action="/welcome" method="post">
            @csrf
            <label for="FirstName">First Name:</label><br>
            <input type="text" name="firstname" id="FirstName"><br><br>
            <label for="LastName">Last Name:</label><br>
            <input type="text" name="lastname" id="LastName"><br><br>

            <label>Gender:</label><br>
            <input type="radio" id="Male" name="Gender"><label for="Male">Male</label><br>
            <input type="radio" id="Female" name="Gender"><label for="Female">Female</label><br>
            <input type="radio" id="Other" name="Gender"><label for="Other">Other</label><br><br>
           
            <label>Nationality:</label><br>
            <select name="nationality">
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>
            </select><br><br>
            
            <label>Language Spoken:</label><br>
            <input type="checkbox" id="Bindo"><label for="Bindo">Bahasa Indonesia</label><br>
            <input type="checkbox" id="Bing"><label for="Bing">English</label><br>
            <input type="checkbox" id="other"><label for="other">Other</label><br><br>
            
            <label>Bio:</label><br>
            <textarea name="bio" rows="10" cols="30"></textarea><br>

            <button type="submit">Sign Up</button>

        </form>
    </body>
</html>