@extends('layout.master')

@section('title')
List Data Pemain Film 
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm my-3">Create</a>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($data as $key => $item)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$item -> nama}}</td>
          <td>
            <form action="/cast/{{$item->id}}" method="POST">
              <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
              @method('delete')
              @csrf
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">
            </form>
          </td>
        </tr>
    @empty
        <h1>Data Kosong</h1>
    @endforelse
    
  </tbody>
</table>
@endsection

